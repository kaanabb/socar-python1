import pandas as pd

import numpy as np

from sklearn.preprocessing import StandardScaler

from sklearn.metrics import mean_squared_error

from sklearn.linear_model import LinearRegression


 

rvp_rawdf = pd.read_excel(r"/opt/app-root/src/RVPDATA.xlsx")

rvp_data = rvp_rawdf.drop(["Dtime","Dtime2"], axis = 1)[:771]

rvp_data.drop(index = rvp_data.loc[rvp_data["110SCB1001_DIST40"] > 90].index, inplace = True)


 

#Find and remove obvious differences between Calculated RVP and Experimental RVP

rvp_comp_data = rvp_data[["110SCB1001_RVP_KPA","RVP_Calc"]]

rvp_comp_data["110SCB1001_RVP_KPA"] - rvp_comp_data["RVP_Calc"]

rvp_comp_data.insert(2,"Diff",np.abs(rvp_comp_data["110SCB1001_RVP_KPA"] - rvp_comp_data["RVP_Calc"]))

rvp_data.drop(rvp_comp_data.loc[rvp_comp_data["Diff"] > 30].index, inplace = True)


 

#Clear operational outlier data

rvp_data.drop(rvp_data.loc[rvp_data["100TI0147.PV"] < 137].index, inplace = True)


 

rvp_op_list = rvp_data.corr().sort_values(by = "110SCB1001_RVP_KPA", ascending = False)

op_list = list(rvp_op_list[0:18].index)

exp_data = rvp_data[op_list].drop(["RVP_Calc"], axis = 1)


 

#Get validation data

rvp_valid_data = rvp_rawdf.drop(["Dtime","Dtime2"], axis = 1)[771:]

exp_data2 = rvp_valid_data[op_list].drop(["RVP_Calc"], axis = 1)






lr = LinearRegression()

sc = StandardScaler()

#train_data = exp_data.sample(frac = 0.5, random_state = 2761) ##==best dataset value so far==##

train_data = exp_data.sample(frac = 0.13, random_state = 3474)

test_data = exp_data.drop(train_data.index)

x_train, y_train = train_data.drop("110SCB1001_RVP_KPA", axis=1), train_data["110SCB1001_RVP_KPA"]

features = x_train.columns.tolist()

sc.fit(x_train)

x_train_scaled = sc.transform(x_train)

lr.fit(x_train_scaled,y_train)


 

val_x, val_y = exp_data2.drop("110SCB1001_RVP_KPA", axis=1), exp_data2["110SCB1001_RVP_KPA"]

val_x_scaled = sc.transform(val_x)

val_x["Actual"] = val_y

val_x["Predicted_LR"] = lr.predict(val_x_scaled)

print("Validation Score: " + str(mean_squared_error(val_y, lr.predict(val_x_scaled))))
